package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sajid/data"
	"github.com/sajid/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GetAllUsers(response http.ResponseWriter, request *http.Request) {
	println("handle get all users request")

	users, err := db.GetAllUsers()
	if err != nil {
		http.Error(response, err.Error(), 500)
	}

	encoder := json.NewEncoder(response)
	encoder.Encode(users)
}

func GetUser(response http.ResponseWriter, request *http.Request) {
	println("handle get a user request")

	id, _ := getIdFromRequest(request)
	user, err := db.GetUserFromId(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusNotFound)
	}

	encoder := json.NewEncoder(response)
	encoder.Encode(&user)
}

func CreateUser(response http.ResponseWriter, request *http.Request) {
	println("handle create user")

	var user data.User
	json.NewDecoder(request.Body).Decode(&user)
	user.Validate()

	result, err := db.CreateUser(user)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
	}
	json.NewEncoder(response).Encode(result)
}

func UpdateUser(response http.ResponseWriter, request *http.Request) {
	println("handle update user")

	id, _ := getIdFromRequest(request)

	// read updated user from request body
	var user data.User
	json.NewDecoder(request.Body).Decode(&user)
	user.Validate()

	err := db.UpdateUser(user, id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusBadRequest)
	}

	json.NewEncoder(response).Encode(user)
}

func DeleteUser(response http.ResponseWriter, request *http.Request) {
	println("handle delete request")

	id, _ := getIdFromRequest(request)
	err := db.DeleteUser(id)
	if err != nil {
		http.Error(response, err.Error(), http.StatusNotFound)
		return
	}
}

func getIdFromRequest(request *http.Request) (primitive.ObjectID, error) {
	params := mux.Vars(request)
	return primitive.ObjectIDFromHex(params["id"])
}
