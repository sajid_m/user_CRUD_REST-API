User CRUD built in Golang and MongoDB

End points are:

GET -    http://localhost:8081/v1/users
GET -    http://localhost:8081/v1/users/{id}
POST -   http://localhost:8081/v1/users
PUT -    http://localhost:8081/v1/users/{id}
DELETE - http://localhost:8081/v1/users/{id}