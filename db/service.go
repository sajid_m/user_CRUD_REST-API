package db

import (
	"context"
	"errors"
	"time"

	"github.com/sajid/data"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var collection = ConnectDB()

func GetAllUsers() (*[]data.User, error) {
	filter := bson.M{"active": true}
	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	var users []data.User

	err = cur.All(context.TODO(), &users)
	if err != nil {
		return nil, err
	}

	return &users, nil
	// for cur.Next(context.TODO()) {
	// 	var user data.User

	// 	err := cur.Decode(&user)

	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}

	// 	users = append(users, user)
	// }
}

func GetUserFromId(id primitive.ObjectID) (*data.User, error) {
	var user data.User

	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&user)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, errors.New("user does not exist")
	}

	return &user, nil
}

func CreateUser(user data.User) (*mongo.InsertOneResult, error) {
	user.Created = time.Now()
	user.Updated = time.Now()
	user.Active = true
	result, err := collection.InsertOne(context.TODO(), user)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func UpdateUser(user data.User, id primitive.ObjectID) error {
	// filter document
	filter := bson.M{"_id": id}

	// updated model
	update := bson.D{
		{"$set", bson.D{
			{"updated", time.Now()},
			{"firstName", user.FirstName},
			{"lastName", user.LastName},
			{"age", bson.D{
				{"value", user.Age.Value},
				{"interval", user.Age.Interval},
			}},
			{"mobile", user.Mobile},
		}},
	}

	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&user)
	if err != nil {
		return err
	}

	return nil
}

func DeleteUser(id primitive.ObjectID) error {
	// filter document
	filter := bson.M{"_id": id}
	// update document
	update := bson.D{
		{"$set", bson.D{
			{"active", false},
		}},
	}

	var user data.User
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&user)
	if err != nil {
		return err
	}

	return nil
}
